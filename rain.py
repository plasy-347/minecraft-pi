# Abychom mohli začít programovat Minecraft:
from mcpi.minecraft import Minecraft

# Připojíme se na nový svět 
mc = Minecraft.create()

# Zpráva do chatu
# mc.postToChat("Hello world")

# Jeden blok na pozici 0, 10, 0:
# mc.setBlock(0, 10, 0, 1)

# Déšť loučí
# louče lze nahradit, viz: https://www.stuffaboutcode.com/p/minecraft-api-reference.html 
x = 0
z = 0
for vyska in range(0, 50):
    mc.setBlock(x, vyska, z, 50)