# Minecraft Pi - příklady

# Abychom mohli začít programovat Minecraft:
from mcpi.minecraft import Minecraft

# Připojíme ze na nový svět 
mc = Minecraft.create()

# Zpráva do chatu
mc.postToChat("Hello world")

# Jeden blok na pozici 0, 10, 0:
mc.setBlock(0, 10, 0, 1)