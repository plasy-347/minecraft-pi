# Jak změnit skin panáčka v Minecraftu

Skin je umístěn v souboru: `/opt/minecraft-pi/data/images/mob/char.png`

Nový nakopírujeme v Terminálu příkazem např.: `sudo cp /home/pi/Documents/lucy.png /opt/minecraft-pi/data/images/mob/char.png`

Zdroj: https://www.raspberrypi-spy.co.uk/2016/03/how-to-change-your-character-skin-in-minecraft-pi-edition/