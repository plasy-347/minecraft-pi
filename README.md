# Příklady
První příklad: https://gitlab.com/plasy-347/minecraft-pi/-/blob/master/hello-world.py

Déšť loučí: https://gitlab.com/plasy-347/minecraft-pi/-/blob/master/rain.py

# Skin
Změna vzhledu panáčka: https://gitlab.com/plasy-347/minecraft-pi/-/blob/master/change-skin.md

# Zdroje

Zdroj pro porgramování: https://projects.raspberrypi.org/en/projects/getting-started-with-minecraft-pi/4

Seznam kostek: https://www.stuffaboutcode.com/p/minecraft-api-reference.html